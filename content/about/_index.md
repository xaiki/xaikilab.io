+++
title = "About Niv Sardi"
author = ["Niv Sardi"]
lastmod = 2020-06-05T19:36:23-03:00
draft = false
+++

## Bio {#bio}

Hacker militante del campo popular cuyas inquietudes se plasman en
problematicas tan diversas como la politica latinoamericana (Observatorio de
Politicas Culturales del CCC), los medios audiovisuales (WallKinTun TV), el
tango (PSOL), el derecho de autor (Butter Project/Popcorn Time), la investigación
científica (endlessm.com) y el Software Libre (Debian, Linux, FFMPEG)

Matematico de formación se despempeño como ingeniero de I+D (Silicon
Graphics, SmartJog/Television De Francia), director de proyectos
(Alexandria, Popcorn Time), asesor tecnico/politico (INAES, UNQ) y educador
popular (Envión, Chela,…)
