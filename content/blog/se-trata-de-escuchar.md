+++
title = "Se trata de escuchar"
author = ["Niv Sardi"]
date = 2020-06-16T10:51:00-03:00
lastmod = 2020-06-16T10:51:10-03:00
categories = ["blog", "opiniones"]
draft = false
+++

Existe un error fundamental recurrente en la visión que tienen los dirigentes politicos de como se tiene que medir la performance de una campaña en redes sociales: creen que se trata de hablar mas, mas fuerte y mas seguido.

Es un error tan grave como común que encuentra sus echos en los actores que venden consultorias y servicios de campaña, vanagloriando la cantidad de mensajes de whatsapps que logran mandar. Genera campañas que modelan el problema como una carrera de información donde quien logre tirar mas cantidad de estimulos sobre un grupo ganaria.

Existen varios problemas con esa visión vamos por partes.


## Las personas se saturan {#las-personas-se-saturan}

Parece evidente pero si varios actores compiten en la guerra de delugio de
información las casillas y feeds no tardan en llenarse saturando la audiencia de
mensajes contradictorios (que son increcientemente filtrados por algoritmos). En
esos casos una campaña mas effectiva en ancho de banda (i.e. que tiene mejores
dispostivos tecnicos para llegar) puede resultar ser negativa ya que se hace
receptaculo de la frustración de saturación por ambos bandos.

Es una tecnica conocida de campaña negativa, generar disgusto por los metodos o
mensajes del oponente permite captar simpatia a su lado, si las falsas encuestas
telefonicas para desprestigiar un oponente funcionan dramaticamente bien[^fn:1],
las llamadas de madrugada con mensajes del candidato opuesto tambien han
demostrado ser un metodo efectivo de campaña porque lleva esa frustración a una
invasión de las esferas personales.

Nuestra percepción de la privacidad en el espacio digital cambia dramaticamente
de una applicación a otra, acceptamos la publicidad en audio de spotify,
toleramos las publicaciones sponsorisadas en Facebook pero nos iritan los
contactos indeseados en WhatsApp e Instagram.

Desconocer estas particularidades y pensar que una estrategia de delugio digital
en WhatsApp se mide por cantidad de mensajes enviados es un error grave que
lleva varias estrategias de campaña a ganarse una rejección express.


## Es mas fácil agregar comunidades que crearlas {#es-mas-fácil-agregar-comunidades-que-crearlas}

En 2018 Bolsonaro creaba la sorpesa creciendo virtiginosamente a la cima de un
paisaje politico brasilero fragementado y tensionado al rededor de un centraõ
desarticulado con un Temer criticado y un Cunha preso. Del otro lado, una
maniobra de lawfare privaba sin pruebas a Lula de su libertad y el PT no lograba
definir una linea ni un candidato claro hasta muy avanzado el proceso electoral.

Tumulto a parte, la tradición politica brasilera permitía esperar una lucha
entre el banquero de Lula y el gobernador de São Paulo. Sin embargo la elección
llevaria 2 sorpresas: Jair Bolsonaro y el Cabo Daciolo, ex-PSOL[^fn:2] (izquierda
radical) que multiplicara casi por 3 los resultados de su ultimo partido sin
ninguna clase de apoyo, dejando atras al candidato del PMDB Henrique Meirelles.

La victoria de Bolsonao fue explicada por una alianza secreta con Cambridge
Analytica, una interferencia de la CIA, un apoyo encubierto de las iglesias
evangelicas,… y muchas mas teorias que no voy a ahondar en esta nota. Sin
embargo ninguna de esas teorias logra explicar el excelente resultado de Daciolo
ni el pesimo desempeño comparativo de Boulos. Las pujas Haddad/Bolsonaro y
Boulos/Daciolo son comparables y las razones del excito tambien.

Mientras las izquierdas brasileras intentaban imponer el discurso y crear
comunidades, las derechas identificaban comunidades existentes a las cuales
daban voces y se hacian de sus discursos, en una estrategia muy similar al «Be
water» Hong Kongita[^fn:3]. Esta estrategia culminaria con una entrevista
telivisiva donde Bolsonaro tenia escrito en su mano[^fn:4], esos temas fueron
dictados por las comunidades que el equipo de campaña monitoreaba por whatsapp,
el lider se hacia echo de las comunidades. Al mismo tiempo, el PT lanzaba una
plataforma de gamificación de la militancia que proponia recompensas por
instalar temas y crear comunidades, desperdiciando asi una base humana mas
grande en tareas que resultaron contra productivas para la elección.


## Las redes sociales son una excelente fuente y un pesimo destino {#las-redes-sociales-son-una-excelente-fuente-y-un-pesimo-destino}

Es un error pensar que 'las redes sociales' son un reflejo de la sociedad. En
primer lugar, una gran parte de la población no tiene acceso. Por otro lado son
muy distintas las poblaciones de Twitter, Facebook, Tik Tok o Fortnite.
Finalmente las personas se conectan a esas redes con deseos distintos, una misma
persona no busca lo mismo de su experiencia en Reddit que en Instagram.

El modelo de negocios de las redes tambien es distinto, mientras Whatsapp
construia un negocio millonario que se basa unicamente en la venta de un grafo
social intimo[^fn:5]<sup>, </sup>[^fn:6] y un crecimiento vertiginoso Twitter debió esperar Q1
2018 para reportar sus primeras ganancias[^fn:7], a 14 años despues de su
creación.

¿ Porque fue tan dificil construir el modelo de negocio de la por lacual hoy
@POTUS gobierna ? Parte de la respuesta viene de una particularidad: en Twitter
no se puede dirigir un mensaje. Effectivamente durante mucho tiempo el servicio
se comportaba como una LIFO[^fn:8], lo que no estaba en la imediatez temporal de
la consulta del usuario se perdia para siempre. ¿ Como entonces vender espacios
de anuncio ? Para esa tarea Twitter tuvo que matar su ecosistema de
desarrolladores restringiendo el uso de sus servicios programaticos (API) para
pivotear de un modelo de base de datos como servicio a ser una App con un
cliente único.

La historia comercial de Twitter es emblematica de un cambio de paradigma en la
valoración del mercado de datos personales. Hoy la red vende principalmente el
accesso irrestricto a sus datos[^fn:9], entendiendo que vale mas escuchar que
hablar en redes.

Otras redes tambien hicieron ese cambio aunque desde un lugar mas sútil, lo que
vende Facebook a demas de publicidad es publicidad microdirigida, no solo
colocan contenido frente a ojos sino que venden la selección de deseos
personales y su alineación con el producto propuesto. Escuchan para mapear
posibilidades.

En materia de comunicación politica lo local tiene un tremendo impacto, en unas
sociedades donde las poblaciones se sienten incrementamente distanciadas de 'lo
politico', las demostraciones de sensibilidad local (sobre explotadas en mostrar
como comen los candidatos[^fn:10]) son focos importantes de la comunicación. Si
se usan como soporte de inteligencia, las redes pueden ser una gran herramienta
para entender las preocupaciones locales y dar respuestas mas empaticas y
dirigidas a las preocupaciones.


## Sin metricas no hay campaña {#sin-metricas-no-hay-campaña}

Punto elemental aunque tragicamente olvidado, no se trata solamente de tener
metricas, pero de verificar empiracamente que estamos midiendo algo que tenga
sentido para nuestro objetivo. Poco sirve mandar 10 millones de twitts diarios
si nadie los lee, como lo vimos en el punto anterior puede hasta ser contra
producente. La articulación constante entre sólidos equipos de ciencias de datos
y sociologia applicada que pueda retroalimentar los agentes de comunicación y
articulación es un requisito inaludible.

En su desposición[^fn:11] frente a la camara de MPs en inglaterra, el ex-director
de analysis de Cambridge Analytica explica con amplios detalles como miden la
effectividad de una operación: miden la «activación positiva» osea cuantas de
las personas en el grupo de interes tomaron una acción en respuesta a la
comunicación. Puede ser inscribirse a una cadena de mail, asistir a un evento,
hacer una donación, es mas importante la metrica de activacón que la acción en
si.

Ese metodo cientifico de medición de comunicaciones es una piedra pivotal de la
effectividad, permite refinar al mismo tiempo la comunicación que los modelos
sociologicos y los mismos puntos de datos usados.

Mover esas interacciones y mediciones al espacio digital simplifica, abarata y
acorta el ciclo de retroalimentaciön. Es evidente que es muchisimo mas rapido y
barato consultar una lista de correos creada que tener que armar un focus group
pre y post acción. Tambien permite regar experimentos en varias comunidades sin
contaminar una poblaciõn mas extensa de una comunicación tóxica.

Apprender es acceptar transformarse, una comunicación que no pivotea es una
comunicación que fracasa. Sin metricas claras y ágiles esas transformaciones
suelen llegar demasiado tarde.

[^fn:1]: <https://m.infonews.com/politica/la-campana-contra-filmus-quedo-impune-n191099>
[^fn:2]: es interesante nota que el candidato del PSOL Guiherme Boulos estaba asistido por quien se veia como el inexplicable nuevo fenomeno digital: Midia Ninja, que a pesar de poner toda sur red al servicio de la campaña no pudo subir ningun digito a las proyecciones de voto pre-campaña e hicieron una peor elección que daciolo con su auto y su biblia.
[^fn:3]: <https://www.independent.co.uk/news/world/asia/hong-kong-protest-latest-bruce-lee-riot-police-water-a9045311.html>
[^fn:4]: <https://nossapolitica.net/2018/08/bolsonaro-leva-cola-na-mao/>
[^fn:5]: Si bien existe encrypción el modelo de negocio se basa sobre la red de contactos que se infiere de la agenda y la frequencia de comunicacion.
[^fn:6]: Los mensajes de **texto** en WhatsApp estan cifrados con claves unicas, pero los mensajes multimedia se cifran con claves comunes que permiten reducir la carga en servidores y tracear tendencias
[^fn:7]: <https://www.cnbc.com/2018/02/08/twitter-q4-2017-earnings-revenue.html>]
[^fn:8]: Last In First Out: en programación es una lista que se descarga 'por arriba' como una pila de libros
[^fn:9]: <https://brightplanet.com/2013/06/25/twitter-firehose-vs-twitter-api-whats-the-difference-and-why-should-you-care/>
[^fn:10]: <https://www.politico.com/magazine/story/2019/03/17/2020-candidates-food-225814>
[^fn:11]: <https://m.youtube.com/watch?v=X5g6IJm7YJQ>
