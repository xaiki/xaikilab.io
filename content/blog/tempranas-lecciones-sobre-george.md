+++
title = "Tempranas lecciones sobre George"
author = ["Niv Sardi"]
date = 2020-06-03T00:00:00-03:00
publishDate = 2020-06-03T00:00:00-03:00
lastmod = 2020-06-06T10:58:44-03:00
categories = ["blog", "opiniones"]
draft = false
type = "post"
+++

Escribo estas palabras acompañado de un velador que contrasta el apagón de
una Casa Blanca fantasmagorica. En 6 noches, las protestas por la impunidad
de los asesinos de George Loyd en Minneapolis fueron ardiendo por las calles
norteamericanas hasta llegar, literalmente, a iluminar de llamas las
vecindades de la residencia presidencial. Trump tuvo que apagar la luz y
salir a refugiarse en algun bunker obscuro del dispositivo de seguridad
nacional.

Los eventos que desatan movimientos sociales siempre me fascinaron, George
Loyd agrega su nombre a una larguisima lista de victimas de la violencia
racial (y social) de la historia moderna estadunidense, hasta sus ultimas
palabras resonan tragicamente con las de Eric Garner, sufocado a muerte en
2014 por varios agentes de la NYPD. ¿ Será «la angustia frente a la recesión
economica y al coronavirus» como analisa Jason Miller, asesor de Trump en la
Casa Blanca[^fn:1] ?

Una cosa queda en evidencia: es posible organizarse y rebelarse en el pais
con la mayor ingerencia en telecomunicaciones en el mundo. Si bien los
ventos siguen ocurriendo y tendremos que medir en unas semanas, unos años
las consecuencias que tendran estos levantamientos, siento que existen
tempranas lecciones que son interesantes analizar.


## La encripción funciona {#la-encripción-funciona}

Es evidente que las agencias de inteligencias norteamericanas estan
vigilando la población y tienen una capacidad de analisis e intercepción
instalada que supera ampliamente cualquier otro pais del mundo. Es obvio que
a pesar de decir lo contrario siguen haciendo espionage interior y que
tienen accesos privilegiados a las distintas redes «sociales» que operan en
su territorio. Si bien existen folkloricas peleas entre GAFAs y el FBI, se
sabe desde la era Snowden que celebran con el gobierno acuerdos bastante
explicitos de colaboración.

Frente a esta situación, hace años que varios colectivos anarquistas y de
derechos humanos empujan el uso de encripción fuerte en los distintos
movimientos sociales en EEUU. Por suerte estan hoy en dia en extincion les
dirigentes politicos que no hayan escuchado hablar de la red de
comunicaciones Tor o de la app de mensageria instantanea cifrada Signal.

La clara incapacidad de las fuerzas represivas de dimensionar las propuestas
e interferir en las comunicaciones de les organizadores, como lo hicieron
numerosas veces en el pasado[^fn:2], permite estimar que los dispositivos de
seguridad usados por una gran mayoria de personas organizando y coordinando
las manifestaciones está funcionando.


## las empresas que controlan las redes sociales no pueden silenciar todo {#las-empresas-que-controlan-las-redes-sociales-no-pueden-silenciar-todo}

La encrypción es una explicación necesaria a la viabilidad de las protestas
pero no es suficiente: ademas de una articulación por redes seguras de las
conducciones, la masividad es debida al uso de todos los medios de difusión,
entre ellos redes sociales clasicas como Facebook, Instagram o Twitter.

Es evidente que las empresas dueñas de esas redes tienen los medios
tecnologicos de indexar y bloquear esas comunicaciones, es tambien logico
que exista desde el poder ejecutivo estadunidense una fuerte presión para
hacerlo. ¿ Porque será entonces que no están llegando masivamente reportes
de bloqueo ? ¿ Será que aprendieron del papelon que hicieron hace casi 10
años con las protestas a BART&nbsp;[^fn:3] ?

Otro vector de explicación se materializa en la creciente simpatia de los
sectores de ingenieria de esas empresas frente a causas sociales lo que
dificulta el encubrimiento interno de esos intentos de censura. En los
ultimos años la politisación del personal tecnico de las GAFAs fue
evidenciado por varias protestas en temas tan diversos como privacidad[^fn:4]
o derechos laborales de otras areas[^fn:5]. La dificultad de emplear y formar
cuadros tecnicos y su creciente involucramiento en cuestiones sociales pesa
cada vez mas fuertemente en las decisiones politicas de unas direcciones que
se esfuerzan para reorganizar esa tensión como excusa para una lavada cara
en materia de defensa de libertades individuales.

[^fn:1]: <https://www.politico.com/news/2020/05/31/white-house-divided-trump-racial-tensions-293042>
[^fn:2]: <https://www.theregister.com/2017/07/26/standing%5Frock%5Fprotester%5Fsurveillance/> <https://www.nytimes.com/2020/06/01/us/george-floyd-protests-live-updates.html#floyd-protests-map-embed>
[^fn:3]: <https://www.eff.org/deeplinks/2011/08/bart-pulls-mubarak-san-francisco>
[^fn:4]: <https://www.buzzfeednews.com/article/carolineodonovan/google-dragonfly-maven-employee-protest-demands>
[^fn:5]: <https://www.bloomberg.com/news/articles/2019-07-08/amazon-workers-plan-prime-day-strike-despite-15-an-hour-pledge> Es importante notar que ese despertar politico pesa de manera muy desigual en cuestiones locales que en internacionalidades marcadas de solidos prejuicios que acrecentan medios de comunicaciones y productos culturales. En este sentido, no es evidente que movimientos sociales en nuestras latitudes esten tan protegidos por la gracia de los hacedores de tecnologia.
