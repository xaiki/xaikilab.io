+++
title = "propuesta de charlas"
author = ["Niv Sardi"]
lastmod = 2020-09-28T19:19:43-03:00
categories = ["blog", "opiniones", "charlas"]
draft = false
+++

## Identidad Digital {#identidad-digital}

Con el COVID-19 se esta borrando el limite entre nuestros seres fisicos y
digitales, esto levanta un par de problemas intersantes


### Legalidad {#legalidad}

Vemos con cada vez mas frecuencia titulares como: "Se detuvo a un twittero por
sus comentarios en la red…", ahi existen varios problemas del orden macro:

-   definir si las comunicaciones digitales son del ambito privado o publico y que parte
    (i.e. ¿es lo mismo desear la muerte de alguien en whatsapp que en twitter?)
-   como inferrir el sentido del humor/poetico en las comunicaciones digitales
    (i.e. ¿es lo mismo 'la voy a matar jajaja', que 'la voy a matar!' ?)
    (ver caso Estado Australiano VS The Juice Media)
-   garantizar que la persona fisica acusada sea la persona virtual
    (i.e. ¿se puede garantizar que no fue hackeado ?)


### Derechos {#derechos}

Si es que nuestro ser digital es parte de nuestra sociedad, entonces podemos
inferir que los derechos humanos garantizados por la CUDDHH y las
constituciones nacionales deben estar garantizadas **EN EL AMBITO DIGITAL**.

Obviamente el primer derecho es el derecho a «ser» pero en lo cibernetico esto
requiere acceso a un aparato y a una connexión ambos pagos. Obviamente
podriamos contra-argumentar que el estado no garantiza el derecho a la vida
(por no actuar en casos de gatillo facil o de violencia de genero, no
garantizar comida y vivienda etc…)

Pero existen tambien derechos a la cultura, a la propiedad privada (sic,
violados por las plataformas cuando monetizan los contenidos producidos por los
usuarios sin dejarles una posibilidad de disentir)

A esos derechos, se suman derechos particulares, como el derecho al olvido, a
la desconexion... y derechos que faltan como el derecho a la usabilidad.


## Relaciones Politicas {#relaciones-politicas}

Si bien el derecho internacional tiene reglas claras para poder incidir
rapidamente en crisis trasnacionales en terminos juridicos (pirateria, trafico
de persona, violación de propiedad intelectual), faltan dramaticamente en las
altas esferas de la diplomacia mundial (ONU/UNESCO/…) una sensibilidad sobre
los temas digitales


### Soberania Nacional {#soberania-nacional}

Exite una transversalidad entre el reporte del WSJ sobre la innaccion de facebook frente a la
violencia xenofoba en india[^fn:1], y el mas reciente reporte de Sophie Zang
sobre[^fn:2] o mas mundanamente las tragicas condiciones laborales de los
repartidores de Rappi y Giovo: Los gigantes de la tecnologia influyen sobre los
bienestares nacionales y es cuasi-imposible para las naciones poder hacer
respetar su ley.

-   ¿ facebook/mercadolibre tienen mas data que afip/anses/indec ?
-   ¿ si son un vehiculo de proyeccion de dinero a poder, como los regulamos ?


### Batalla Cultural {#batalla-cultural}

En la casi totalidad del planeta existen leyes para garantizar que los medios
de comunicación **deban** incluir contenido nacional en su difusión (mataria que
en ese 'nacional' se transforme en 'plurinacional' y que de una ves por todas
se integren los pueblos a los derechos, pero es otro debate). Sin embargo,
frente a las platafomas de contenido esto se cesga tremendamente. Un
ex-director de ANCINE (brazil) me contaba que cuando una pelicula iba a ser un
fiasco la programaba en 2-3 cines conocidos para que netflix compre los
derechos, lo hacen para cumplir con una ley, pero al final tienen la 'quota' de
contenido nacional de pesima calidad que nadie ve e universalizan el mensaje.

Hay una cuestion mas profunda en la cual no ahondare que es sobre los codigos
intrinsecuos a las obras culturales (razón por lacual en la pos-guerra todos
los paises del mundo desarrollaron ministerios de la cultura con el fin de
desvincular el poder economico de la formación de opinión popular)


## La Brecha {#la-brecha}

Esa palabra de 'brecha digital' me satura, pero es la que esta instalada. Que
es la brecha: sino la distancia en la experiencia que viven los que mas tienen
y los que menos ? Pero, si las GAFAs son gratis de que estamos hablando ? de la
conectividad ? de la capacidad de procesamiento de los aparatos ? de la calidad
de las pantallas ? de la resolucione del mouse ?


### Contenido y presentación {#contenido-y-presentación}

uno de los problemas centrales a esa brecha es la ligatura indelebil entre
contenido y presentación: solo se puede ver instagram en su app o en su web.
Entonces si la app estan pesadas, necesitas un celular mas reciente para
acceder.


### Interoperabilidad {#interoperabilidad}

Pero si el contenido es nuestro, que es lo que hace que desde GMAIL pueda
escribirle a alguien que tiene YAHOO pero que no pueda ver los vivos de youtube
en facebook o en instagram ?

Los estandares, y la interoperabilidad que fueron la base del internet se
rompieron para poder crear parques privados donde las plataformas ponen las
reglas.


### El aire {#el-aire}

Se va a denotar la edad, pero se acuerdan de los ciber cafes ? ya vieron un
router wifi ? entonces tienen el concepto de que se pueden hacer redes
'populares' y usarlas sin deberle nada a nadie (a parte de EDESUUUUUUUUR).
Entonces porque no pasa lo mismo con las redes mas generales ? simple: el marco
legal, hoy tenemos tecnologia (SDR) que nos permite hacer celdas 3-4g 'en casa'
pero no las podemos sacar a la calle porque es ilegal.

[^fn:1]: <https://www.wsj.com/amp/articles/facebook-hate-speech-india-politics-muslim-hindu-modi-zuckerberg-11597423346>
[^fn:2]: <https://www.buzzfeednews.com/article/craigsilverman/facebook-ignore-political-manipulation-whistleblower-memo>
