baseurl                 = "https://xaiki.gitlab.io"
DefaultContentLanguage  = "es"
title                   = "Niv Sardi"
theme                   = "hugo-future-imperfect-dark"
preserveTaxonomyNames   = true
paginate                = 15
disqusShortname         = ""
googleAnalytics         = ""
pluralizeListTitles     = false
# disableLanguages        = ["fr","pl"]

[params]
  # Loads CSS and JavaScript files. The variable is an array so that you can load
  # multiple/additional files if necessary. The standard theme files can be loaded
  # by adding the value, "default". Default includes the add-on.css and and-on.js.
  # Example: ["default", "/path/to/file"]
  cssFiles              = ["default"]
  jsFiles               = ["default"]
  # Sets options for highlight.js
  highlightjs           = true
  highlightjsTheme      = "github"
  highlightjsLang       = ["json", "toml", "css", "html", "shell", "js"]
  # Sets where "View More Posts" links to
  viewMorePostsLink     = "/blog/"
  # Activate Estimated Reading Times, which appear in the post headers
  readingTime           = true
  # Sets which Social Share links appear in posts.
  # Options are twitter, facebook, reddit, linkedin, stumbleupon, pinterest, email
  socialShare           = ["twitter", "mastodon", "email"]

  [params.meta]
    # Sets the meta tag description
    description         = "Suenos electronicos de un nerd perpetuamente desterrado"
    # Sets the meta tag author
    author              = "xaiki"
    # If you would like to add more comprehensive favicon support passed root
    # directory favicon.ico, utlize this funtion. It is recommened to use
    # https://realfavicongenerator.net to generate your icons as that is the basis
    # for the theme's naming.
    favicon             = true
    svg                 = true
    faviconVersion      = "1"
    msColor             = "#f4f4f4" # Copy from the https://realfavicongenerator.net
    iOSColor            = "#f4f4f4" # Copy from the https://realfavicongenerator.net

  [params.header]
    # Sets the navbarTitle that appears in the top left of the navigation bar
    navbarTitle         = "Niv Sardi"
    # Sets navbarTitle to match the section of the website
    dynamicTitles       = true
    searchMenu          = true
    shareMenu           = true
    languageMenu        = false

  # These are optional params related to the sidebar. They are recommended, but not
  # required for proper functionality. HTML is supported within the params.
  [params.intro]
    header                = "Niv Sardi"
    paragraph             = "<large>Hack</large> | Tech | Opiniones"
    rssIntro              = true
    socialIntro           = true

    # This appears at the top of the sidebar above params.intro.header.
    # A width of less than 100px is recommended from a design perspective.
    [params.intro.pic]
      src                 = "https://www.gravatar.com/avatar/9cddcb9b135cb4794ac422f5f7faee0d"
      # Masks image in a certain shape. Supported are circle, triangle, diamond, and hexagon.
      shape               = "circle"
      width               = "100px"
      alt                 = "Niv Sardi"

  [params.sidebar]
    about               = """
    Hacker militante del campo popular cuyas inquietudes se plasman en
problematicas tan diversas como la politica latinoamericana, los medios audiovisuales, el
tango, el derecho de autor, la investigación
científica y el Software Libre"""

    # Sets the number of recent posts to show in the sidebar. The default value is 5.
    postAmount          = 5
    # set to show or to hide categories in the sidebar
    categories          = true
    # Sets Categories to sort by number of posts instead of alphabetical
    categoriesByCount   = true

  [params.footer]
    # Sets RSS icons to appear on the sidebar with the social media buttons
    rssFooter           = true
    # Sets Social Media icons to appear on the sidebar
    socialFooter        = true

[menu]
  # Sets the menu items in the navigation bar
  # Identifier prepends a Font Awesome icon to the menu item
  [[menu.main]]
    name              = "Home"
    url               = "/"
    identifier        = "fas fa-home"
    weight            = 1

  [[menu.main]]
    name              = "About"
    url               = "/about/"
    identifier        = "far fa-id-card"
    weight            = 2

  [[menu.main]]
    name              = "Blog"
    url               = "/blog/"
    identifier        = "far fa-newspaper"
    weight            = 3

  [[menu.main]]
    name              = "Contact"
    url               = "/contact/"
    identifier        = "far fa-envelope"
    weight            = 6

[Languages]
  # Each language has its own menu.
  [Languages.es]
    languageCode        = "es"
    LanguageName        = "Castellano"
    weight              = 1

  [Languages.fr]
    languageCode        = "fr"
    LanguageName        = "Français"
    title               = "Hugo Future Imperfect Dark in fr"
    description         = "Un thème de HTML5 UP, porté par Julio Pescador. Slimmed et amélioré par Patrick Collins. Multilingue par StatnMap. Propulsé par Hugo. "
    weight              = 2

    [[Languages.fr.menu.main]]
      name              = "Accueil"
      url               = "/"
      identifier        = "fas fa-home"
      weight            = 1

    [[Languages.fr.menu.main]]
      name              = "About"
      url               = "/about/"
      identifier        = "far fa-id-card"
      weight            = 2

    [[Languages.fr.menu.main]]
      name              = "Blog"
      url               = "/blog/"
      identifier        = "far fa-newspaper"
      weight            = 3

    [[Languages.fr.menu.main]]
      name              = "Catégories"
      url               = "/categories/"
      identifier        = "fas fa-sitemap"
      weight            = 5

    [[Languages.fr.menu.main]]
      name              = "Contact"
      url               = "/contact/"
      identifier        = "far fa-envelope"
      weight            = 6

  [Languages.en]
    languageCode        = "en"
    LanguageName        = "English"
    title               = "Hugo Future Imperfect in en"
    description         = "A theme from HTML5 UP, ported by Julio Pescador, Slimmed and improved by Patrick Collins.  Multilang by StatnMap. Powered par Hugo."
    weight              = 2

    [[Languages.en.menu.main]]
      name              = "Home"
      url               = "/"
      identifier        = "fas fa-home"
      weight            = 1

    [[Languages.en.menu.main]]
      name              = "About"
      url               = "/about/"
      identifier        = "far fa-id-card"
      weight            = 2

    [[Languages.en.menu.main]]
      name              = "Blog"
      url               = "/blog/"
      identifier        = "far fa-newspaper"
      weight            = 3

    [[Languages.pl.menu.main]]
      name              = "Categories"
      url               = "/categories/"
      identifier        = "fas fa-sitemap"
      weight            = 5

    [[Languages.pl.menu.main]]
      name              = "Contact"
      url               = "/contact/"
      identifier        = "far fa-envelope"
      weight            = 6

# Sets Social Media icons to appear and link to your account. Value should be your
# username unless otherwise noted. These are the icons affected by socialAppearAtTop
# and socialAppearAtBottom.
[social]
  # Coding Communities
  github                = "xaiki"
  gitlab                = "xaiki"
  stackoverflow         = "" # User Number
  bitbucket             = ""
  jsfiddle              = ""
  codepen               = ""
  # Visual Art Communities
  deviantart            = ""
  flickr                = ""
  behance               = ""
  dribbble              = ""
  # Publishing Communities
  wordpress             = ""
  medium                = ""
  # Professional/Business Oriented Communities
  linkedin              = ""
  linkedin_company      = ""
  foursquare            = ""
  xing                  = ""
  slideshare            = ""
  # Social Networks
  facebook              = ""
  googleplus            = ""
  reddit                = ""
  quora                 = ""
  youtube               = ""
  vimeo                 = ""
  whatsapp              = "" # WhatsApp Number
    # WeChat and QQ need testing.
    wechat              = ""
    qq                  = "" # User ID Number
  instagram             = ""
  tumblr                = ""
  twitter               = "xaiki"
  skype                 = ""
  snapchat              = ""
  pinterest             = ""
  telegram              = "xaiki"
  vine                  = ""
  googlescholar         = ""
  orcid                 = ""
  researchgate          = ""
  # Email
  email                 = "x at btn dot sh"
